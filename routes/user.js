const express = require("express");
const router = express.Router();

const {
  getAllUsers,
  getUser,
  addUser,
  updateUser,
  deleteUser
} = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

//Get all users
router.get("/", async (req, res, next) => {
  const result = await getAllUsers();

  if (result) {
    res.send(result);
  } else {
    res.status(400).send("Some error");
  }
});

//Get user
router.get("/:id", async (req, res, next) => {
  const result = await getUser(req.params.id);

  if (result) {
    res.send(result);
  } else {
    res.status(400).send("Some error");
  }
});

//Add user
router.post("/", isAuthorized, async (req, res, next) => {
  const result = await addUser(req.body);

  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});

//Update user
router.put("/:id", isAuthorized, async (req, res, next) => {
  const result = await updateUser(req.params.id, req.body);

  if (result) {
    res.send(result);
  } else {
    res.status(400).send(`Some error`);
  }
});

//Delete user
router.delete("/:id", isAuthorized, async (req, res, next) => {
   const result = await deleteUser(req.params.id);

   if (result) {
     res.send(`User was deleted - ${result}`);
   } else {
     res.status(400).send(`Some error`);
   }
});

module.exports = router;
