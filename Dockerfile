FROM node:11.2.0-alpine
WORKDIR /app
COPY . ./
RUN npm i

CMD ["node", "./bin/www.js"]
EXPOSE 3000