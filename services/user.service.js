const { User } = require("../models/user");
const {
  getAll,
  get,
  add,
  update,
  remove
} = require("../repositories/user.repository");

const getNextId = async () => {
  const users = await getAll();
  let currIndex = parseInt(users.pop()._id);
  return currIndex ? ++currIndex + "" : "0";
};

const getAllUsers = async () => {
  return getAll();
};

const getUser = async id => {
  if (id) {
    return get(id);
  } else {
    return true;
  }
};

const addUser = async data => {
  if (data) {
    try {
      const user = new User(data);
      user._id = await getNextId();
      const res = add(user);
      return res ? user : false;
    } catch (error) {
      return false;
    }
  } else {
    return false;
  }
};

const updateUser = async (id, data) => {
  if (data && id) {
    try {
      const user = new User(data);
      user._id = id + "";
      const res = update(user);
      return res ? user : false;
    } catch (error) {
      return false;
    }
  } else {
    return false;
  }
};

const deleteUser = async id => {
  if (id) {
    return remove(id);
  } else {
    return true;
  }
};

module.exports = {
  getAllUsers,
  getUser,
  addUser,
  updateUser,
  deleteUser
};
