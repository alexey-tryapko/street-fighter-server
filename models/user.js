class User {
  constructor({ name, health, attack, defense, source }) {
    if (!(name && health && attack && defense && source))
      throw new Error("All fields are not transmitted");
    this._id = "";
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.source = source;
  }

  static checkInt(value) {
    return typeof value === "number" ? true : false;
  }
}

module.exports = { User };
