const { storage } = require("../repositories/general.repository");

const usersStorage = storage("userList.json");

const getAll = async () => {
  const data = await usersStorage.getAllData();
  return data || false;
};

const get = async id => {
  if (id) {
    const data = await usersStorage.getDataById(id);
    return data || false;
  } else {
    return false;
  }
};

const add = async data => {
  if (data) {
    const res = await usersStorage.addData(data);
    return res || false;
  } else {
    return false;
  }
};

const update = async data => {
  if (data) {
    const res = await usersStorage.updateData(data);
    return res || false;
  } else {
    return false;
  }
};

const remove = async id => {
  if (id) {
    const res = await usersStorage.deleteData(id);
    return res || false;
  } else {
    return false;
  }
};

module.exports = {
  getAll,
  get,
  add,
  update,
  remove
};
