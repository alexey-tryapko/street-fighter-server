# Server for "Street Fighter"

## Link

https://street-fighter-srvr.herokuapp.com


## Instalation

`npm install`

`npm start`

Server will start on http://localhost:3000/

## Client

[Demo](https://alexeytryapko.github.io/street-fighter/)

[Source code](https://bitbucket.org/alexey-tryapko/street-fighter/src/master/)

## Endpoints

#### User Resources
- **GET /user**
- **GET /user/:id**
- **POST /user**
- **PUT /user/:id/followers**
- **DELETE /user/:id**

----
### Get all users

  Returns json data with all users.

* **URL**

    https://street-fighter-srvr.herokuapp.com/user

* **Method:**

    `GET`
  
* **URL Params**

    None

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 

    **Content:**  
```json
[{
    "_id": "1",
    "name": "Ryu",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
},
{
    "_id": "2",
    "name": "Dhalsim",
    "health": 60,
    "attack": 3,
    "defense": 1,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
},
{
    "_id": "3",
    "name": "Guile",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif"
}]
```
### Get user by id

    Returns json data with user.

* **URL**

    https://street-fighter-srvr.herokuapp.com/user/:id

* **Method:**

    `GET`
  
* **URL Params**

    `id=[integer]`

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200

    **Content:**  
```json
{
    "_id": "1",
    "name": "Ryu",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
}
```
### Add user

    Add new user

* **URL**

    https://street-fighter-srvr.herokuapp.com/user

* **Method:**

    `POST`
  
* **URL Params**

    None

* **Data Params**

    Authorization

* **Success Response:**

  * **Code:** 200

    **Content:**  
```json
{
    "_id": "20",
    "name": "Ryu",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
}
```
### Update user

    Update esisting user

* **URL**

    https://street-fighter-srvr.herokuapp.com/user/:id

* **Method:**

    `PUT`
  
* **URL Params**

    `id=[integer]`

* **Data Params**

    Authorization

* **Success Response:**

  * **Code:** 200 

    **Content:**  

```json
{
    "_id": "20",
    "name": "Ryu",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
}
```
### Delete user

    Delete user

* **URL**

    https://street-fighter-srvr.herokuapp.com/user/:id

* **Method:**

    `DELETE`
  
* **URL Params**

    `id=[integer]`

* **Data Params**

    Authorization

* **Success Response:**

  * **Code:** 200 

    **Content:**  
    ```
    User was deleted - true
    ```
